package com.testtech.app.fragments.question;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.model.Question;

public class OpenEndedQuestionFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		// Get patched-through data
		final Bundle args = this.getArguments();
		final int position = args.getInt(BundleKeyset.TT_SURVEY_POSITION, 0);
		final int total = args.getInt(BundleKeyset.TT_SURVEY_TOTAL, 0);
		final Question question = ((Question) (args.getSerializable(BundleKeyset.TT_SURVEY_QUESTION)));

		final View v = inflater.inflate(R.layout.survey_open_question, container, false);

		final TextView title = ((TextView) (v.findViewById(R.id.title)));
		title.setText(question.getFlavorText());
		final TextView count = ((TextView) (v.findViewById(R.id.question_no)));
		count.setText("Question " + position + " of " + total);

		return v;
	}
}
