package com.testtech.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.testtech.app.R;
import com.testtech.app.model.User;
import com.testtech.app.views.AccountView;

public class AccountPageFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.login_page, container, false);
	}

	public void setAccount(User account) {

		final AccountView account_form = ((AccountView) (this.getView().findViewById(R.id.account)));
		account_form.setUserData(account);
	}

}
