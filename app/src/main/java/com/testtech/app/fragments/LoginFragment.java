package com.testtech.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.User;
import com.testtech.app.model.UserDao;

import java.util.ArrayList;
import java.util.List;

public class LoginFragment extends Fragment {

	private final static class AccountPagerAdapter extends FragmentStatePagerAdapter {

		private List<User> users;

		public AccountPagerAdapter(FragmentManager fm, List<User> users) {

			super(fm);
			this.users = users;

			if (this.users == null)
				this.users = new ArrayList<>(0);
		}

		@Override
		public Fragment getItem(int position) {

			final Fragment account = new AccountPageFragment();
			if (position == this.users.size())
				return account;

			((AccountPageFragment) (account)).setAccount(this.users.get(position));
			return account;
		}
		@Override
		public int getCount() {
			return (this.users.size() + 1);
		}

	}
	private final static class AccountPageTransformer implements ViewPager.PageTransformer {

		@Override
		public void transformPage(View page, float position) {
			page.setRotationY(position * -30);
		}

	}

	private ViewPager pager;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Set basic fragment parameters for persistence and UI configuration
		this.setRetainInstance(true);
		this.setHasOptionsMenu(true);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		this.pager = ((ViewPager) (inflater.inflate(R.layout.login, container, false)));

		// Load stored user data (if any)
		final ProgressDialog dialog = new ProgressDialog(this.getActivity());
		dialog.setIndeterminate(true);
		dialog.setMessage(this.getActivity().getString(R.string.loading_users));
		dialog.show();

		final DaoMaster dao = TestTechnology.getActivity().getDatabase();
		final UserDao users = dao.newSession().getUserDao();
		List<User> accounts = null;

		if (users.count() > 0) {

			accounts = users.queryBuilder()
					.limit(5)
					.orderAsc(UserDao.Properties.Id)
					.list();
		}
		else
			accounts = new ArrayList<>(0);

		dialog.dismiss();

		// Retrieve any previous positional ID stored
		final int position = (savedInstanceState != null ? savedInstanceState.getInt(BundleKeyset.TT_LOGIN_POSITION, 0) : 0);

		// Set account pages and return the pager view.
		this.pager.setAdapter(new AccountPagerAdapter(this.getChildFragmentManager(), accounts));
		this.pager.setCurrentItem(position, true);
		this.pager.setPageTransformer(false, new AccountPageTransformer());

		return this.pager;
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);

		final int position = this.pager.getCurrentItem();
		outState.putInt(BundleKeyset.TT_LOGIN_POSITION, position);
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.login, menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final FragmentManager manager = this.getFragmentManager();

		switch (item.getItemId()) {

			case R.id.menu_server_settings: {

				final Fragment f = new ServerFragment();
				manager.beginTransaction()
					   .replace(R.id.container, f, "server")
					   .addToBackStack("login->server")
					   .commit();

				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

}
