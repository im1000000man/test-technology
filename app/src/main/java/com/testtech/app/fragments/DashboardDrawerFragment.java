package com.testtech.app.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.Profile;
import com.testtech.app.model.User;

import de.greenrobot.dao.DaoException;

public class DashboardDrawerFragment extends Fragment {

	private static enum DrawerEntry {

		ACTION_EDIT_PROFILE (R.string.edit_profile, R.drawable.ic_action_edit),
		ACTION_SETTINGS (R.string.settings, R.drawable.ic_action_settings),
		ACTION_TUTORIAL (R.string.tutorial, R.drawable.ic_action_help),
		ACTION_ABOUT (R.string.about_this_app, R.drawable.ic_action_about),
		ACTION_SUPPORT (R.string.support, R.drawable.ic_action_group),
		ACTION_RATE_RECOMMEND (R.string.rate_recomment, R.drawable.ic_action_favorite);

		private int resource;
		private int icon;

		DrawerEntry(int resource, int icon) {

			this.resource = resource;
			this.icon = icon;
		}

		public int getResource() {
			return this.resource;
		}
		public int getIcon() {
			return this.icon;
		}

	}
	private final static class DrawerListAdapter extends ArrayAdapter<DrawerEntry> {

		public DrawerListAdapter(Context context) {
			super(context, R.layout.drawer_list_item, DrawerEntry.values());
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null)
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_list_item, parent, false);

			final DrawerEntry entry = this.getItem(position);
			convertView.setTag(entry);

			final TextView option = ((TextView) (convertView.findViewById(R.id.option)));
			option.setText(entry.getResource());

			final ImageView icon = ((ImageView) (convertView.findViewById(R.id.icon)));
			icon.setImageResource(entry.getIcon());

			return convertView;
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Set basic fragment parameters for persistence and UI configuration
		this.setRetainInstance(true);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		final View drawer = inflater.inflate(R.layout.drawer, container, false);
		final User session = TestTechnology.getActivity().getSession();

		if (session != null) {

			// Set account briefing: fill with data and set actions
			this.setAccountBriefing(drawer, session);

			// Set drawer options
			final ListView options = ((ListView) (drawer.findViewById(R.id.options)));
			options.setAdapter(new DrawerListAdapter(this.getActivity()));
			options.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

					try {

						final FragmentManager manager = TestTechnology.getActivity()
																	  .getSupportFragmentManager();

						final DrawerEntry entry = ((DrawerEntry) (view.getTag()));
						switch (entry) {

							case ACTION_EDIT_PROFILE: {

								final Fragment f = new EditProfileFragment();
								TestTechnology.getActivity().lockDrawer();
								manager.beginTransaction()
									   .replace(R.id.container, f, "profile_page")
									   .addToBackStack("dashboard->profile_page")
									   .commit();

								break;
							}
						}
					}
					catch (ClassCastException e) { Log.e("Test Technology", "Unexpected value stored in tag at click."); }
				}

			});
		}

		return drawer;
	}

	private void setAccountBriefing(View drawer, User session) {

		// Set account briefing
		final TextView username = ((TextView) (drawer.findViewById(R.id.username)));
		username.setText(session.getUsername());
		final TextView user_full_name = ((TextView) (drawer.findViewById(R.id.user_full_name)));

		Profile profile;
		try { profile = (session.getProfileId() >= 0L ? session.getProfile() : null); }
		catch (DaoException e) { profile = null; }

		final String full_name = (profile != null ? (profile.getFirstName() + ' ' + profile.getLastName()) : "Unknown");
		user_full_name.setText(full_name);

		// Set logout button action: prompt if should logout, then do it
		final ImageButton logout = ((ImageButton) (drawer.findViewById(R.id.logout)));
		logout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				final Context context = DashboardDrawerFragment.this.getActivity();
				final DialogInterface.OnClickListener accept = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						TestTechnology.getActivity().endSession();
						dialog.dismiss();
					}

				};
				final DialogInterface.OnClickListener cancel = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				};
				final AlertDialog dialog = new AlertDialog.Builder(context).setTitle(R.string.logout_confirm)
																		   .setMessage(R.string.logout_confirm_text)
																		   .setPositiveButton(R.string.accept, accept)
																		   .setNegativeButton(R.string.cancel, cancel)
																		   .create();

				dialog.show();
			}

		});
	}

}
