package com.testtech.app.fragments.question;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.fragments.SurveyFragment;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.Option;
import com.testtech.app.model.OptionDao;
import com.testtech.app.model.Question;

import java.util.List;

public class SingleChoiceQuestionFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		// Get patched-through data
		final Bundle args = this.getArguments();
		final int position = args.getInt(BundleKeyset.TT_SURVEY_POSITION, 0);
		final int total = args.getInt(BundleKeyset.TT_SURVEY_TOTAL, 0);
		final Question question = ((Question) (args.getSerializable(BundleKeyset.TT_SURVEY_QUESTION)));

		final View v = inflater.inflate(R.layout.survey_single_choice, container, false);

		final TextView title = ((TextView) (v.findViewById(R.id.title)));
		title.setText(question.getFlavorText());
		final TextView count = ((TextView) (v.findViewById(R.id.question_no)));
		count.setText("Question " + position + " of " + total);

		final RadioGroup options_group = ((RadioGroup) (v.findViewById(R.id.options)));
		final DaoMaster dao = TestTechnology.getActivity().getDatabase();
		final OptionDao options = dao.newSession().getOptionDao();

		final List<Option> options_list = options.queryBuilder()
												 .where(OptionDao.Properties.QuestionId.eq(question.getId()), OptionDao.Properties.SurveyId.eq(question.getSurveyId()))
												 .list();

		for (final Option o : options_list) {

			final RadioButton option = new RadioButton(this.getActivity());
			option.setText(o.getFlavorText());

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
				option.setId(View.generateViewId());
			else
				option.setId(SurveyFragment.generateViewId());

			option.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					o.setValue(Boolean.toString(isChecked));
				}

			});

			options_group.addView(option, new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
		}

		return v;
	}
}
