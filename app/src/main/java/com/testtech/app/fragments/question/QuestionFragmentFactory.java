package com.testtech.app.fragments.question;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.testtech.app.BundleKeyset;
import com.testtech.app.model.Question;
import com.testtech.app.model.QuestionType;

public class QuestionFragmentFactory {

	private final static QuestionFragmentFactory FACTORY = new QuestionFragmentFactory();

	public Fragment create(Question q, QuestionType type, int position, int total) {

		final Fragment f;
		final Bundle args = new Bundle();
		args.putInt(BundleKeyset.TT_SURVEY_POSITION, position);
		args.putInt(BundleKeyset.TT_SURVEY_TOTAL, total);
		args.putSerializable(BundleKeyset.TT_SURVEY_QUESTION, q);

		switch (type.getType()) {

			case "single-choice": { f = new SingleChoiceQuestionFragment(); break; }
            case "multiple-choice": { f = new MultipleChoiceQuestionFragment(); break; }
            case "open-ended": { f = new SingleChoiceQuestionFragment(); break; }
			default: { f = null; break; }

		}

		if (f != null)
			f.setArguments(args);

		return f;
	}

	public static QuestionFragmentFactory getInstance() {
		return QuestionFragmentFactory.FACTORY;
	}

}
