package com.testtech.app.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.Profile;
import com.testtech.app.model.User;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;

public class GeneralProfileFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		final RelativeLayout card_right = ((RelativeLayout) (inflater.inflate(R.layout.profile_general, container, false)));
		final User session = TestTechnology.getActivity().getSession();
		final Profile profile = session.getProfile();

		final EditText first_name = ((EditText) (card_right.findViewById(R.id.first_name)));
		first_name.setText(profile.getFirstName() != null ? profile.getFirstName() : "");

		final EditText last_name = ((EditText) (card_right.findViewById(R.id.last_name)));
		last_name.setText(profile.getLastName() != null ? profile.getLastName() : "");

		final RadioButton male = ((RadioButton) (card_right.findViewById(R.id.gender_male)));
		final RadioButton female = ((RadioButton) (card_right.findViewById(R.id.gender_female)));
		male.setSelected(profile.getGender() == Profile.GENDER_MALE);
		female.setSelected(profile.getGender() == Profile.GENDER_FEMALE);

		final EditText birth_date = ((EditText) (card_right.findViewById(R.id.birth_date)));
		birth_date.setText(this.dateToString(profile, new DateTime()));
		birth_date.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				final DateTime d = new DateTime();
				final int year = d.getYear();
				final int month = d.getMonthOfYear();
				final int day = d.getDayOfMonth();

				final DatePickerDialog dialog = new DatePickerDialog(GeneralProfileFragment.this.getActivity(), new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int month, int day) {

						final LocalDate date = new LocalDate(year, month, day);
						profile.setBirthDate(date.toDateTimeAtStartOfDay().toDate());
						birth_date.setText(GeneralProfileFragment.this.dateToString(profile, date.toDateTimeAtStartOfDay()));
					}

				}, year, month, day);
				dialog.show();
			}

		});

		final Button save_profile = ((Button) (card_right.findViewById(R.id.save_profile)));
		save_profile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!first_name.getText().toString().isEmpty())
					profile.setFirstName(first_name.getText().toString());
				if (!last_name.getText().toString().isEmpty())
					profile.setLastName(last_name.getText().toString());

				if (male.isSelected())
					profile.setGender(Profile.GENDER_MALE);
				else
					profile.setGender(Profile.GENDER_FEMALE);

				// TODO: Save this into local database and try to send to remote server.
				// TODO: If this does not work, use a timer broadcast to attempt again in 5 minutes.

				// TODO: Disabled for now, as this may not work due to hardcoding. Make sure to add this in release.
			}

		});

		return card_right;
	}
	public String dateToString(Profile profile, DateTime now) {
		return new SimpleDateFormat("EEE, d MMM yyyy").format(profile.getBirthDate() != null ? profile.getBirthDate() : now.toDate());
	}

}
