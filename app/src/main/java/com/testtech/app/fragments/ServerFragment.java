package com.testtech.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testtech.app.R;

import java.util.Date;

public class ServerFragment extends Fragment {

	public final static class ServerKey {

		private String key;
		private Date added;

		public ServerKey(String key, long added) {

			this.key = key;
			this.added = new Date(added);
		}

		public String getKey() {
			return this.key;
		}
		public Date getDateAdded() {
			return this.added;
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Set basic fragment parameters for persistence and UI configuration
		this.setRetainInstance(true);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.server, container, false);
	}

}
