package com.testtech.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testtech.app.R;
import com.testtech.app.views.ProfileCardView;

public class EditProfileFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		final ProfileCardView profile = ((ProfileCardView) (inflater.inflate(R.layout.profile, container, false)));
		profile.getViewPager().setAdapter(new ProfileCardView.ProfileCardAdapter(this.getChildFragmentManager()));
		profile.getViewPager().setCurrentItem(1);

		return profile;
	}

}
