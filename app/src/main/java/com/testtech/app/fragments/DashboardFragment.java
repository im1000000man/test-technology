package com.testtech.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.DaoSession;
import com.testtech.app.model.Filter;
import com.testtech.app.model.FilterDao;
import com.testtech.app.model.User;
import com.testtech.app.net.FilterListSyncTask;
import com.testtech.app.net.ProfileSyncTask;
import com.testtech.app.views.SwipeFilterPageView;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

	private SwipeFilterPageView swipe;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Set basic fragment parameters for persistence and UI configuration
		this.setRetainInstance(true);
		this.setHasOptionsMenu(true);

		// Get user profile_page or create a new one.
		// TODO: If the profile_page is new, launch the tutorial.
		final User session = TestTechnology.getActivity().getSession();
		if (session != null && session.getProfileId() == 0L) {

			final Settings settings = TestTechnology.getActivity().getSettings();

			// Load profile_page from remote branch (if any).
			final ProfileSyncTask profile_task = new ProfileSyncTask(this.getActivity(), (settings.getServer().getHostURL() + "profile_page/" + session.getUsername()), session);
			profile_task.execute(
				new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
				new BasicNameValuePair("server-key", settings.getServer().getDeviceServerKey()),
				new BasicNameValuePair("user-id", "" + session.getId())
			);

			// Load filters from remote branch (if any).
			final FilterListSyncTask filter_task = new FilterListSyncTask(this.getActivity(), (settings.getServer().getHostURL() + "filter-list/" + session.getUsername()), session);
			filter_task.execute(
				new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
				new BasicNameValuePair("server-key", settings.getServer().getDeviceServerKey()),
				new BasicNameValuePair("user-id", "" + session.getId())
			);
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		TestTechnology.getActivity().unlockDrawer();

		// Load filters from remote and local database
		final ProgressDialog dialog = new ProgressDialog(this.getActivity());
		dialog.setIndeterminate(true);
		dialog.setMessage(this.getActivity().getString(R.string.loading_users));
		dialog.show();

		final DaoMaster dao = TestTechnology.getActivity().getDatabase();
		final DaoSession session = dao.newSession();
		final FilterDao filters = session.getFilterDao();
		List<Filter> filter_list = null;

		if (filters.count() > 0) {

			filter_list = filters.queryBuilder()
								 .where(FilterDao.Properties.UserId.eq(TestTechnology.getActivity().getSession().getId()))
								 .orderAsc(FilterDao.Properties.Id)
								 .list();
		}
		else
			filter_list = new ArrayList<>(0);

		// Create the default filter (ALL filter)
		final Filter default_filter = new Filter(0L, TestTechnology.getActivity().getSession().getId(), "all", "");
		default_filter.__setDaoSession(session);
		filter_list.add(0, default_filter);

		dialog.dismiss();

		this.swipe = ((SwipeFilterPageView) (inflater.inflate(R.layout.dashboard, container, false)));
		this.swipe.getViewPager().setAdapter(new SwipeFilterPageView.FilterPageAdapter(this.getChildFragmentManager(), this.swipe, filter_list));

		return this.swipe;
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.dashboard, menu);
	}
	@Override
	public void onPrepareOptionsMenu(Menu menu) {

		super.onPrepareOptionsMenu(menu);
		final MenuItem swap_views = menu.findItem(R.id.menu_swap_views);

		if (swap_views != null && this.swipe != null) {

			if (this.swipe.getPageLayoutMode() == SwipeFilterPageView.LAYOUT_GRID)
				swap_views.setIcon(R.drawable.ic_action_view_as_list);
			else
				swap_views.setIcon(R.drawable.ic_action_view_as_grid);
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final FragmentManager manager = this.getFragmentManager();

		switch (item.getItemId()) {

			case R.id.menu_swap_views: {

				if (this.swipe.getPageLayoutMode() == SwipeFilterPageView.LAYOUT_GRID) {

					item.setIcon(R.drawable.ic_action_view_as_list);
					this.swipe.setPageLayoutMode(SwipeFilterPageView.LAYOUT_LIST);
				}
				else {

					item.setIcon(R.drawable.ic_action_view_as_grid);
					this.swipe.setPageLayoutMode(SwipeFilterPageView.LAYOUT_GRID);
				}

				Log.i("Test Technology", "Swapping view (using-grid: " + this.swipe.getPageLayoutMode() + "): invalidating pager.");
				return true;
			}
			case R.id.menu_filters: {

				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

}
