package com.testtech.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.fragments.question.QuestionFragmentFactory;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.Question;
import com.testtech.app.model.QuestionDao;
import com.testtech.app.model.QuestionType;
import com.testtech.app.model.QuestionTypeDao;
import com.testtech.app.model.Survey;
import com.testtech.app.model.SurveyDao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class SurveyFragment extends Fragment {

	private final static class QuestionPageTransformer implements ViewPager.PageTransformer {

		private static final float MIN_SCALE = 0.85f;
		private static final float MIN_ALPHA = 0.5f;

		public void transformPage(View view, float position) {

			int pageWidth = view.getWidth();
			int pageHeight = view.getHeight();

			if (position < -1)
				view.setAlpha(0);
			else if (position <= 1) {

				float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
				float vertMargin = pageHeight * (1 - scaleFactor) / 2;
				float horzMargin = pageWidth * (1 - scaleFactor) / 2;
				if (position < 0)
					view.setTranslationX(horzMargin - vertMargin / 2);
				else
					view.setTranslationX(-horzMargin + vertMargin / 2);

				view.setScaleX(scaleFactor);
				view.setScaleY(scaleFactor);

				view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA));
			}
			else
				view.setAlpha(0);
		}

	}
	private final static class QuestionPageAdapter extends FragmentStatePagerAdapter {

		private Fragment start;
		private Fragment end;
		private List<Question> questions;

		public QuestionPageAdapter(FragmentManager fm, final Survey s, List<Question> questions) {

			super(fm);
			this.questions = questions;

			if (this.questions == null)
				this.questions = new ArrayList<>(0);

			final List<Question> _questions = this.questions;

			this.start = new Fragment() {

				@Override
				public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

					final View v = inflater.inflate(R.layout.survey_begin, container, false);
					final TextView title = ((TextView) (v.findViewById(R.id.title)));
					title.setText(s.getTitle());

					final TextView author = ((TextView) (v.findViewById(R.id.author)));
					author.setText("by " + s.getAuthor());

					final TextView description = ((TextView) (v.findViewById(R.id.description)));
					description.setText(s.getDescription());

					final TextView count = ((TextView) (v.findViewById(R.id.question_count)));
					int required = 0;

					for (Question q : _questions) {

						if (q.getRequired())
							required++;
					}
					final int optional = (_questions.size() - required);

					count.setText("Contents: " + required + " required questions, " + optional + " optional questions.");

					return v;
				}
			};
			this.end = new Fragment() {

				@Override
				public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

					final View v = inflater.inflate(R.layout.survey_end, container, false);
					final Button submit = ((Button) (v.findViewById(R.id.submit_survey)));
					submit.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {

							// TODO: Here, we should send a AAJAX request with the data for saving, but we'll omit it for now.

							// The survey is answered: mark it as so and register.
							s.setAnswered(true);

							final DaoMaster dao = TestTechnology.getActivity().getDatabase();
							final SurveyDao surveys = dao.newSession().getSurveyDao();

							surveys.insertOrReplaceInTx(s);
							final FragmentManager manager = TestTechnology.getActivity().getSupportFragmentManager();
							manager.popBackStackImmediate();
						}

					});

					return v;
				}

			};
		}

		@Override
		public Fragment getItem(int position) {

			if (position == 0)
				return this.start;
			else if (position == (this.getCount() - 1))
				return this.end;

			final DaoMaster dao = TestTechnology.getActivity().getDatabase();
			final QuestionTypeDao types = dao.newSession().getQuestionTypeDao();

			final Question q = this.questions.get(position - 1);
			final List<QuestionType> type_data = types.queryBuilder()
													  .where(QuestionTypeDao.Properties.Id.eq(q.getTypeId()))
													  .limit(1)
													  .list();

			if (type_data != null && !type_data.isEmpty()) {

				final QuestionFragmentFactory factory = QuestionFragmentFactory.getInstance();
				return factory.create(q, type_data.get(0), position, this.questions.size());
			}

			return null;
		}
		@Override
		public int getCount() {
			return (this.questions.size() + 2);
		}

	}

	private final static AtomicInteger NEXT_GEN_ID = new AtomicInteger(1);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		TestTechnology.getActivity().lockDrawer();

		final Bundle args = this.getArguments();
		final Survey s = ((Survey) (args.getSerializable(BundleKeyset.TT_SURVEY_DATA)));

		if (!s.getAnswered() || s.getMultipleAttemptsPermitted()) {

			final DaoMaster dao = TestTechnology.getActivity().getDatabase();
			final QuestionDao questions = dao.newSession().getQuestionDao();
			final List<Question> questions_list = questions.queryBuilder()
														   .where(QuestionDao.Properties.SurveyId.eq(s.getId()))
														   .list();

			final ViewPager pager = ((ViewPager) (inflater.inflate(R.layout.survey, container, false)));
			pager.setPageTransformer(false, new QuestionPageTransformer());
			pager.setAdapter(new QuestionPageAdapter(this.getChildFragmentManager(), s, questions_list));

			return pager;
		}
		else {

			final View v = inflater.inflate(R.layout.survey_complete, container, false);
			final TextView title = ((TextView) (v.findViewById(R.id.title)));
			title.setText(s.getTitle());

			final TextView author = ((TextView) (v.findViewById(R.id.author)));
			author.setText("by " + s.getAuthor());

			final TextView description = ((TextView) (v.findViewById(R.id.description)));
			description.setText(s.getDescription());

			return v;
		}

	}

	public static int generateViewId() {

		for ( ; ; ) {

			final int result = SurveyFragment.NEXT_GEN_ID.get();

			int new_value = (result + 1);
			if (new_value > 0x00FFFFFF)
				new_value = 1;

			if (SurveyFragment.NEXT_GEN_ID.compareAndSet(result, new_value))
				return result;
		}
	}

}
