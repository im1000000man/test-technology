package com.testtech.app.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.User;
import com.testtech.app.net.PasswordChangeTask;
import com.testtech.app.net.UserDataSyncTask;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.message.BasicNameValuePair;

public class PersonalProfileFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		final RelativeLayout card_left = ((RelativeLayout) (inflater.inflate(R.layout.profile_personal, container, false)));
		final User session = TestTechnology.getActivity().getSession();
		final String old_username = session.getUsername();

		// Set data handles for left side
		final EditText username = ((EditText) (card_left.findViewById(R.id.username)));
		username.setText(session.getUsername());

		final EditText email = ((EditText) (card_left.findViewById(R.id.email)));
		email.setText(session.getEmail());

		final EditText passwd = ((EditText) (card_left.findViewById(R.id.passwd)));
		passwd.setTypeface(Typeface.DEFAULT);
		passwd.setTransformationMethod(new PasswordTransformationMethod());

		final EditText confirm_passwd = ((EditText) (card_left.findViewById(R.id.confirm_passwd)));
		confirm_passwd.setTypeface(Typeface.DEFAULT);
		confirm_passwd.setTransformationMethod(new PasswordTransformationMethod());

		final Button save_profile = ((Button) (card_left.findViewById(R.id.save_profile)));
		save_profile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				final Settings settings = TestTechnology.getActivity().getSettings();
				if (!passwd.getText().toString().isEmpty()) {

					final String new_passwd = passwd.getText().toString();
					final String passwd_confirmation = confirm_passwd.getText().toString();

					if (new_passwd.equals(passwd_confirmation)) {

						final String hash = new String(Hex.encodeHex(DigestUtils.sha1(new_passwd)));

						final PasswordChangeTask passwd_change = new PasswordChangeTask(PersonalProfileFragment.this.getActivity(), (settings.getServer().getHostURL() + "user/passwd/" + session.getUsername()), session);
						passwd_change.execute(
							new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
							new BasicNameValuePair("server-key", settings.getServer().getDeviceServerKey()),
							new BasicNameValuePair("user-id", "" + session.getId()),
							new BasicNameValuePair("passwd", passwd_change.encode(hash))
						);
					}
				}

				boolean has_changed = false;
				String new_username = null;
				String new_email = null;

				if (!username.getText().toString().equals(session.getUsername())) {

					has_changed = true;
					new_username = username.getText().toString();
				}

				if (!email.getText().toString().equals(session.getEmail())) {

					has_changed = true;
					new_email = email.getText().toString();
				}

				if (has_changed) {

					final UserDataSyncTask user_sync = new UserDataSyncTask(PersonalProfileFragment.this.getActivity(), (settings.getServer().getHostURL() + "user/sync/" + old_username), session, new_username, new_email);
					user_sync.execute(
						new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
						new BasicNameValuePair("server-key", settings.getServer().getDeviceServerKey()),
						new BasicNameValuePair("user-id", "" + session.getId()),
						new BasicNameValuePair("user-email", user_sync.encode(new_email != null ? new_email : session.getEmail())),
						new BasicNameValuePair("user-name", (new_username != null ? new_username : session.getUsername()))
					);
				}
			}

		});

		return card_left;
	}

}
