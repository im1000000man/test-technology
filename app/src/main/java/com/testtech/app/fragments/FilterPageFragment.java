package com.testtech.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.DaoSession;
import com.testtech.app.model.Filter;
import com.testtech.app.model.Option;
import com.testtech.app.model.OptionDao;
import com.testtech.app.model.Question;
import com.testtech.app.model.QuestionDao;
import com.testtech.app.model.QuestionType;
import com.testtech.app.model.QuestionTypeDao;
import com.testtech.app.model.Survey;
import com.testtech.app.model.SurveyDao;
import com.testtech.app.views.SwipeFilterPageView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FilterPageFragment extends Fragment {

	private final static class FilterPageListAdapter extends ArrayAdapter<Survey> {

		public FilterPageListAdapter(Context context, List<Survey> data) {
			super(context, R.layout.page_list_item, data);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null)
				convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.page_list_item, parent, false);

			final Survey s = this.getItem(position);
			convertView.setTag(s);

			final TextView title = ((TextView) (convertView.findViewById(R.id.title)));
			title.setText(s.getTitle());

			final TextView author = ((TextView) (convertView.findViewById(R.id.author)));
			author.setText("by " + s.getAuthor());

			final ImageView status = ((ImageView) (convertView.findViewById(R.id.survey_status)));
			status.setImageResource(s.getAnswered() ? R.drawable.ic_action_accept : R.drawable.ic_action_next_item);

			return convertView;
		}

	}
	private final static class FilterPageGridAdapter extends BaseAdapter {

		private Context context;
		private List<Survey> data;

		public FilterPageGridAdapter(Context context, List<Survey> data) {

			this.context = context;
			this.data = data;
		}

		public Context getContext() {
			return this.context;
		}
		@Override
		public int getCount() {
			return this.data.size();
		}
		public Survey getItem(int position) {
			return this.data.get(position);
		}
		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null)
				convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.page_grid_item, parent, false);

			final Survey s = this.getItem(position);
			convertView.setTag(s);

			final TextView title = ((TextView) (convertView.findViewById(R.id.title)));
			title.setText(s.getTitle());

			final TextView author = ((TextView) (convertView.findViewById(R.id.author)));
			author.setText("by " + s.getAuthor());

			final ImageView status = ((ImageView) (convertView.findViewById(R.id.survey_status)));
			status.setImageResource(s.getAnswered() ? R.drawable.ic_action_accept : R.drawable.ic_action_next_item);

			return convertView;
		}

	}

	private final static LoadingCache<Filter, List<Survey>> CACHE = FilterPageFragment.createCache();

	private SwipeFilterPageView swipe_filter;
	private List<Survey> surveys;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Set basic fragment parameters for persistence and UI configuration
		this.setHasOptionsMenu(true);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		// Get filter from arguments and parse it to get the actual survey list.
		final Bundle args = this.getArguments();

		if (this.surveys == null) {

			final Filter filter = ((Filter) (args.getSerializable(BundleKeyset.TT_FILTERED_LIST_FILTER)));

			try { this.surveys = FilterPageFragment.CACHE.get(filter); }
			catch (ExecutionException e) { this.surveys = new ArrayList<>(0); }

			// TODO: Remove when out of testing.
			if (this.surveys.isEmpty()) {

				final DaoMaster dao = TestTechnology.getActivity().getDatabase();
				final DaoSession session = dao.newSession();
				final SurveyDao surveys = session.getSurveyDao();
				final QuestionDao questions = session.getQuestionDao();
				final QuestionTypeDao types = session.getQuestionTypeDao();
				final OptionDao options = session.getOptionDao();

				final QuestionType t1 = new QuestionType(1L, "single-choice", "A question with various answer options but only one possible response."),
								   t2 = new QuestionType(2L, "multiple-choice", "A question with various answer options which accepts one or more than one answer."),
								   t3 = new QuestionType(3L, "open-ended", "A question which is open ended, or has no specified answer choices.");

				types.insertOrReplaceInTx(t1, t2, t3);

				final Survey s1 = new Survey(1L, "Test", "A test survey added to view survey answering page response.", "Test Technology Team", false, new Date(), false),
							 s2 = new Survey(2L, "Already done test", "A test survey added to view answered page response.", "Test Technology Team", false, new Date(), true);
				surveys.insertOrReplaceInTx(s1, s2);

				final Question q11 = new Question(1L, s1.getId(), t1.getId(), "Single-choice question 1", true),
							   q12 = new Question(2L, s1.getId(), t2.getId(), "Multiple-choice question 1", true),
							   q13 = new Question(3L, s1.getId(), t3.getId(), "Open-ended question 1", false);
				questions.insertOrReplaceInTx(q11, q12, q13);

				final Option o111 = new Option(1L, q11.getSurveyId(), q11.getId(), "Option 1", "1"),
							 o112 = new Option(2L, q11.getSurveyId(), q11.getId(), "Option 2", "2"),
							 o121 = new Option(3L, q12.getSurveyId(), q12.getId(), "Check 1", "1"),
							 o122 = new Option(4L, q12.getSurveyId(), q12.getId(), "Check 2", "2"),
							 o131 = new Option(5L, q13.getSurveyId(), q13.getId(), null, null);
				options.insertOrReplaceInTx(o111, o112, o121, o122);

				this.surveys.add(s1);
				this.surveys.add(s2);
			}
			// TODO: End remove
		}

		// Load the view and set the list (or grid, depending the state), then return everything.
		final byte layout = args.getByte(BundleKeyset.TT_FILTERED_LIST_LAYOUT_MODE);
		final AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

				try {

					final Survey s = ((Survey) (view.getTag()));
					final Fragment f = new SurveyFragment();
					final Bundle args = new Bundle();
					args.putSerializable(BundleKeyset.TT_SURVEY_DATA, s);
					f.setArguments(args);

					final FragmentManager manager = TestTechnology.getActivity().getSupportFragmentManager();
					manager.beginTransaction()
						   .replace(R.id.container, f, "survey")
						   .addToBackStack("dashboard->survey")
						   .commit();
				}
				catch (ClassCastException e) { Log.e("Test Technology", "Unexpected value stored in tag at click."); }
			}

		};
		switch (layout) {

			case SwipeFilterPageView.LAYOUT_GRID: {

				final GridView grid = ((GridView) (inflater.inflate(R.layout.filter_page_grid, container, false)));
				grid.setAdapter(new FilterPageGridAdapter(this.getActivity(), this.surveys));
				grid.setOnScrollListener(new AbsListView.OnScrollListener() {

					@Override
					public void onScrollStateChanged(AbsListView view, int scrollState) { ; }
					@Override
					public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

						final int top_row = (grid.getChildCount() == 0 ? 0 : grid.getChildAt(0).getTop());
						SwipeFilterPageView.getInstance().setEnabled(top_row >= 0);
					}

				});
				grid.setOnItemClickListener(listener);

				return grid;
			}
			case SwipeFilterPageView.LAYOUT_LIST: {

				final ListView list = ((ListView) (inflater.inflate(R.layout.filter_page_list, container, false)));
				list.setAdapter(new FilterPageListAdapter(this.getActivity(), this.surveys));
				list.setOnScrollListener(new AbsListView.OnScrollListener() {

					@Override
					public void onScrollStateChanged(AbsListView view, int scrollState) {
						;
					}

					@Override
					public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

						final int top_row = (list.getChildCount() == 0 ? 0 : list.getChildAt(0).getTop());
						SwipeFilterPageView.getInstance().setEnabled(top_row >= 0);
					}

				});
				list.setOnItemClickListener(listener);

				return list;
			}
			default: return null;
		}
	}

	private static LoadingCache<Filter, List<Survey>> createCache() {

		return CacheBuilder.newBuilder()
						   .removalListener(new RemovalListener<Filter, List<Survey>>() {

							   @Override
							   public void onRemoval(RemovalNotification<Filter, List<Survey>> notification) {

								   final Filter filter = notification.getKey();
								   final List<Survey> surveys = notification.getValue();

								   if (filter != null && surveys != null) {

									   Log.i("Test Technology", "Releasing survey list associated to filter (name: " + filter.getName().toUpperCase() + ").");
									   surveys.clear();
								   }
							   }

						   })
						   .build(new CacheLoader<Filter, List<Survey>>() {

							   @Override
							   public List<Survey> load(Filter key) throws Exception {
								   return key.parseFilter();
							   }

						   });
	}

}
