package com.testtech.app;

public final class BundleKeyset {

	public final static String
		TT_ACTIVITY_SETTINGS = "TestTechnology.$.database-settings",
		TT_ACTIVITY_SESSION = "TestTechnology.$.active-session",
		TT_ACTIVITY_STORED_SESSION = "TestTechnology.$.preferences.active-session",

		TT_LOGIN_POSITION = "LoginFragment.$.view.accounts.position",

		TT_FILTERED_LIST_FILTER = "FilterPageFragment.$.filter",
		TT_FILTERED_LIST_LAYOUT_MODE = "FilterPageFragment.$.view.layout_mode",

		TT_SURVEY_DATA = "SurveyFragment.$.data.survey",
		TT_SURVEY_POSITION = "SurveyFragment.$.data.survey.position",
		TT_SURVEY_TOTAL = "SurveyFragment.$.data.survey.total",
		TT_SURVEY_QUESTION = "SurveyData.$.data.survey.question"
	;

	private BundleKeyset() { ; }

}
