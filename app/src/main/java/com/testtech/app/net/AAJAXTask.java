package com.testtech.app.net;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AAJAXTask extends AsyncTask<NameValuePair, Void, String> {

	public static interface AAJAXResponseListener {

		public void onResponse(Context context, String response);

	}

	public final static int
		HTTP_GET = 0x00000001,
		HTTP_POST = 0x00000002,
		HTTP_PUT = 0x0000004,
		HTTP_DELETE = 0x00000008,
		HTTP_HEAD = 0x00000010,
		HTTP_OPTIONS = 0x00000020,
		HTTP_TRACE = 0x00000040
	;

	private Uri address;
	private Context context;
	private List<Header> headers;
	private List<AAJAXResponseListener> result_listeners;
	private int http_method;

	public AAJAXTask(Context context, String address) {
		this(context, Uri.parse(address));
	}
	public AAJAXTask(Context context, Uri address) {
		this(context, address, AAJAXTask.HTTP_POST);
	}
	public AAJAXTask(Context context, String address, int http_method) {
		this(context, Uri.parse(address), http_method);
	}
	public AAJAXTask(Context context, Uri address, int http_method) {

		super();

		this.context = context;
		this.address = address;
		this.http_method = http_method;
		this.headers = new ArrayList<>();
		this.result_listeners = new ArrayList<>();
	}

	public final Uri getAddress() {
		return this.address;
	}
	public final Context getContext() {
		return this.context;
	}
	public int getHttpMethodType() {
		return this.http_method;
	}
	public List<Header> getHeaders() {
		return Collections.unmodifiableList(this.headers);
	}

	public void setHttpMethodType(int http_method) {
		this.http_method = http_method;
	}

	public void addHeader(Header header) {
		this.headers.add(header);
	}
	public void addResponseListener(AAJAXResponseListener listener) {
		this.result_listeners.add(listener);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
	@Override
	protected final String doInBackground(NameValuePair ... params) {

		try {

			final HttpClient client = new DefaultHttpClient();

			// Set transfer method or default to HTTP POST method.
			HttpRequest request = null;
			final int http_method = this.http_method;
			switch (http_method) {

				case AAJAXTask.HTTP_GET: request = new HttpGet(URI.create(this.address.toString())); break;
				default:
				case AAJAXTask.HTTP_POST: request = new HttpPost(URI.create(this.address.toString())); break;
				case AAJAXTask.HTTP_PUT: request = new HttpPut(URI.create(this.address.toString())); break;
				case AAJAXTask.HTTP_DELETE: request = new HttpDelete(URI.create(this.address.toString())); break;
				case AAJAXTask.HTTP_HEAD: request = new HttpHead(URI.create(this.address.toString())); break;
				case AAJAXTask.HTTP_OPTIONS: request = new HttpOptions(URI.create(this.address.toString())); break;
				case AAJAXTask.HTTP_TRACE: request = new HttpTrace(URI.create(this.address.toString())); break;
			}

			// If we're using the POST or PUT protocols, set the data entity using the variable arguments.
			if (this.methodSupportsEntity(http_method) && params != null && params.length > 0) {

				List<NameValuePair> entity_data = new ArrayList<>(params.length);
				Collections.addAll(entity_data, params);

				((HttpEntityEnclosingRequest) (request)).setEntity(new UrlEncodedFormEntity(entity_data));
			}

			// Set request headers (if any).
			if (!this.headers.isEmpty()) {

				for (Header header : this.headers)
					request.addHeader(header);
			}

			// Execute HTTP request and obtain response.
			final HttpResponse response = client.execute((HttpUriRequest) (request));
			return EntityUtils.toString(response.getEntity());
		}
		catch (IOException e) { Log.wtf("AJAX", e); }

		return "";
	}
	@Override
	protected void onPostExecute(String result) {

		super.onPostExecute(result);

		if (!this.result_listeners.isEmpty()) {

			for (AAJAXResponseListener l : this.result_listeners)
				l.onResponse(this.context, result);
		}
	}

	public String encode(String str){

		byte[] data = new byte[0];

		try { data = str.getBytes("UTF-8"); }
		catch (UnsupportedEncodingException e1) { Log.e("Test Technology", "Could not convert byte array to specified encoding. Operation failed."); }

		return Base64.encodeToString(data, Base64.NO_WRAP);
	}
	private boolean methodSupportsEntity(int http_method) {
		return (http_method == AAJAXTask.HTTP_POST || http_method == AAJAXTask.HTTP_PUT);
	}

}
