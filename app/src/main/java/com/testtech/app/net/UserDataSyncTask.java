package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.User;
import com.testtech.app.model.UserDao;

public class UserDataSyncTask extends AAJAXProgressDialogTask {

	public UserDataSyncTask(Context context, String address, User session, String username, String email) {
		this(context, Uri.parse(address), session, username, email);
	}
	public UserDataSyncTask(Context context, Uri address, final User session, final String username, final String email) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				// If there is a null response, simply bounce back and notify.
				if (response == null || response.isEmpty()) {

					Toast.makeText(context, R.string.error_user_sync, Toast.LENGTH_SHORT).show();
					return;
				}

				final Gson json = new Gson();

				// Try to parse response: on error, absorb it and set to no result.
				boolean success = false;
				try { success = json.fromJson(response, Boolean.class); }
				catch (JsonParseException e) {

					Log.i("Test Technology", "Failed to obtain a response from server request (type: user-sync, username: " + session.getUsername() + ").");
					return;
				}

				if (success) {

					final DaoMaster dao = TestTechnology.getActivity().getDatabase();
					final UserDao users = dao.newSession().getUserDao();

					session.setUsername(username);
					session.setEmail(email);
					users.insertOrReplaceInTx(session);

					Toast.makeText(context, R.string.user_synced, Toast.LENGTH_SHORT).show();
				}
				else
					Toast.makeText(context, R.string.error_user_syncing, Toast.LENGTH_SHORT).show();
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.updating_user));
		dialog.setIndeterminate(true);
	}

}
