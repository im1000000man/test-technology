package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.fragments.ServerFragment;
import com.testtech.app.model.schema.JsonSchema;

import java.lang.reflect.Type;

public class ServerKeyTask extends AAJAXProgressDialogTask {

	private final static class ServerKeySchema implements JsonSchema<ServerFragment.ServerKey> {

		@Override
		public ServerFragment.ServerKey deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

			final JsonObject server_key = json.getAsJsonObject();

			if (!server_key.has("date-registered") || server_key.get("date-registered").getAsLong() == 0L)
				throw new JsonParseException("Illegal server key definition encountered: the field \"date-registered\" must exist.");
			if (!server_key.has("server-key") || !server_key.get("server-key").getAsString().matches("[0-9a-f]{6,40}"))
                throw new JsonParseException("Illegal server key definition encountered: the server key has an invalid format or is missing.");

			return new ServerFragment.ServerKey(server_key.get("server-key").getAsString(), server_key.get("date-registered").getAsLong());
		}
		@Override
		public JsonElement serialize(ServerFragment.ServerKey src, Type typeOfSrc, JsonSerializationContext context) {

			final JsonObject server_key = new JsonObject();
			server_key.add("date-registered", new JsonPrimitive(src.getDateAdded().getTime()));
			server_key.add("server-key", new JsonPrimitive(src.getKey()));

			return server_key;
		}

	}

	public ServerKeyTask(Context context, String address) {
		this(context, Uri.parse(address));
	}
	public ServerKeyTask(Context context, Uri address) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				final Gson json = new GsonBuilder().registerTypeAdapter(ServerFragment.ServerKey.class, new ServerKeySchema())
												   .create();

				// Try to parse response into server key object: on error, report it and continue with a null value.
				ServerFragment.ServerKey key = null;

				try { key = json.fromJson(response, ServerFragment.ServerKey.class); }
				catch (JsonParseException e) { Log.e("Test Technology", "Unable to deserialize device-server key: " + e.getMessage() + "\n" +response.toString()+ "."); }

				if (key != null) {

					Log.i("Test Technology", "Recieved device-server key from server.");
					final Settings settings = TestTechnology.getActivity().getSettings();

					settings.getServer().setDeviceServerKey(key.getKey());
					settings.getServer().setDateRegistered(key.getDateAdded());
					settings.save(context);

					Toast.makeText(context, R.string.server_auth_success, Toast.LENGTH_SHORT).show();
				}
				else {

					Log.e("Test Technology", "No device-server key data received from server: could not authenticate server.");
					Toast.makeText(context, R.string.error_no_key, Toast.LENGTH_SHORT).show();
				}
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.authenticate_server));
		dialog.setIndeterminate(true);
	}

}
