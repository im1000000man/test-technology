package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.User;
import com.testtech.app.model.UserDao;
import com.testtech.app.model.schema.UserSchema;

public class LoginTask extends AAJAXProgressDialogTask {

	public LoginTask(Context context, String address) {
		this(context, Uri.parse(address));
	}
	public LoginTask(Context context, Uri address) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				final Gson json = new GsonBuilder().registerTypeAdapter(User.class, new UserSchema())
												   .create();
                Log.d("TestTech", "Response: " + response.toString());

                // Try to parse response into user: on error, report it and continue with a null value.
				User user = null;
				try { user = json.fromJson(response, User.class); }
				catch (JsonParseException e) { Log.e("Test Technology", "Unable to deserialize user: " + e.getMessage() + "."); }

				if (user != null) {

					final DaoMaster dao = TestTechnology.getActivity().getDatabase();
					final UserDao users = dao.newSession().getUserDao();

					// Insert the user data into the local database. If the user already exists, just update it
					Log.i("Test Technology", "Registered user (id: " + user.getId() + ", username: " + user.getUsername() + ") into local database.");
					users.insertOrReplaceInTx(user);

					// Begin session: use the main activity to perform transaction (no one complains about it)
					Log.i("Test Technology", "Starting session for user (id: " + user.getId() + ", username: " + user.getUsername() + ").");
					TestTechnology.getActivity().beginSession(user);
				}
				else {

					Log.e("Test Technology", "No user data received from server: cannot begin session.");
					Toast.makeText(context, R.string.error_no_user, Toast.LENGTH_SHORT).show();
				}
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.login_ongoing));
		dialog.setIndeterminate(true);
	}

}
