package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.Filter;
import com.testtech.app.model.FilterDao;
import com.testtech.app.model.User;
import com.testtech.app.model.schema.FilterListSchema;

public class FilterListSyncTask extends AAJAXProgressDialogTask {

	public FilterListSyncTask(Context context, String address, User session) {
		this(context, Uri.parse(address), session);
	}
	public FilterListSyncTask(Context context, Uri address, final User session) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				final Gson json = new GsonBuilder().registerTypeAdapter(Filter[].class, new FilterListSchema())
												   .create();

				// Try to parse response into filter array: on error or null reference, synchronize nothing.
				Filter[] filter_list = null;
				try { filter_list = json.fromJson(response, Filter[].class); }
				catch (JsonParseException e) { Log.e("Test Technology", "Unable to deserialize filter list: " + e.getMessage() + "."); }

				if (filter_list != null) {

					final DaoMaster dao = TestTechnology.getActivity().getDatabase();
					final FilterDao filters = dao.newSession().getFilterDao();

					// Insert the user data into the local database. If the user already exists, just update it
					Log.i("Test Technology", "Registered filter list for user (id: " + session.getId() + ", username: " + session.getUsername() + ", length: " + filter_list.length + ") into local database.");
					filters.insertOrReplaceInTx(filter_list);
				}
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.sync_filters));
		dialog.setIndeterminate(true);
	}

}
