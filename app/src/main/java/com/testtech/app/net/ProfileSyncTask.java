package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.testtech.app.R;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.DaoSession;
import com.testtech.app.model.Profile;
import com.testtech.app.model.ProfileDao;
import com.testtech.app.model.User;
import com.testtech.app.model.UserDao;
import com.testtech.app.model.schema.ProfileSchema;

public class ProfileSyncTask extends AAJAXProgressDialogTask {

	public ProfileSyncTask(Context context, String address, User session) {
		this(context, Uri.parse(address), session);
	}
	public ProfileSyncTask(Context context, Uri address, final User session) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				final Gson json = new GsonBuilder().registerTypeAdapter(Profile.class, new ProfileSchema())
												   .create();

				// Try to parse response into profile_page: on error, absorb it and create an empty profile_page.
				Profile profile = null;
				try { profile = json.fromJson(response, Profile.class); }
				catch (JsonParseException e) {

					Log.i("Test Technology", "Creating empty profile_page for account (id: " + session.getId() + ", username: " + session.getUsername() + ").");
					profile = new Profile(session.getId());
				}

				if (profile == null)
					profile = new Profile(session.getId());

				Log.i("Test Technology", "Setting reflective references for user profile_page (username: " + session.getUsername() + ").");
				profile.setUserId(session.getId());
				profile.setUser(session);

				session.setProfile(profile);

				final DaoMaster dao = TestTechnology.getActivity().getDatabase();
				final DaoSession _session = dao.newSession();
				final UserDao users = _session.getUserDao();
				final ProfileDao profiles = _session.getProfileDao();

				// Insert the user data into the local database. If the user already exists, just update it
				if (session.getId() > 0) {

					Log.i("Test Technology", "Registered user profile_page (id: " + profile.getId() + ") into local database.");
					users.insertOrReplaceInTx(session);
					profiles.insertOrReplaceInTx(profile);
				}
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.sync_profile));
		dialog.setIndeterminate(true);
	}

}
