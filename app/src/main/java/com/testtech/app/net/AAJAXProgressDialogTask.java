package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;

public abstract class AAJAXProgressDialogTask extends AAJAXTask {

	private ProgressDialog dialog;

	public AAJAXProgressDialogTask(Context context, String address) {
		this(context, Uri.parse(address));
	}
	public AAJAXProgressDialogTask(Context context, Uri address) {
		this(context, address, AAJAXTask.HTTP_POST);
	}
	public AAJAXProgressDialogTask(Context context, String address, int http_method) {
		this(context, Uri.parse(address), http_method);
	}
	public AAJAXProgressDialogTask(Context context, Uri address, int http_method) {
		super(context, address, http_method);
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();

		this.dialog = new ProgressDialog(this.getContext());
		this.onProgressDialogCreate(this.dialog);
		this.dialog.show();
	}
	@Override
	protected void onPostExecute(String result) {

		this.dialog.dismiss();
		super.onPostExecute(result);
	}

	protected abstract void onProgressDialogCreate(ProgressDialog dialog);


}
