package com.testtech.app.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.testtech.app.R;
import com.testtech.app.model.User;

public class PasswordChangeTask extends AAJAXProgressDialogTask {

	public PasswordChangeTask(Context context, String address, User session) {
		this(context, Uri.parse(address), session);
	}
	public PasswordChangeTask(Context context, Uri address, final User session) {

		super(context, address);
		this.addResponseListener(new AAJAXResponseListener() {

			@Override
			public void onResponse(Context context, String response) {

				// If there is a null response, simply bounce back and notify.
				if (response == null || response.isEmpty()) {

					Toast.makeText(context, R.string.error_password_change, Toast.LENGTH_SHORT).show();
					return;
				}

				final Gson json = new GsonBuilder().create();

				// Try to parse response into profile_page: on error, absorb it and create an empty profile_page.
				boolean success = false;
				try { success = json.fromJson(response, Boolean.class); }
				catch (JsonParseException e) {

					Log.i("Test Technology", "Failed to obtain a response from server request (type: password-change, username: " + session.getUsername() + ").");
					return;
				}

				if (success)
					Toast.makeText(context, R.string.password_changed, Toast.LENGTH_SHORT).show();
				else
					Toast.makeText(context, R.string.error_password_changing, Toast.LENGTH_SHORT).show();
			}

		});
	}

	@Override
	protected void onProgressDialogCreate(ProgressDialog dialog) {

		dialog.setMessage(this.getContext().getString(R.string.setting_new_passwd));
		dialog.setIndeterminate(true);
	}

}
