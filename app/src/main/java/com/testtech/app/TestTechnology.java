package com.testtech.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;

import com.testtech.app.fragments.DashboardDrawerFragment;
import com.testtech.app.fragments.DashboardFragment;
import com.testtech.app.fragments.LoginFragment;
import com.testtech.app.model.DaoMaster;
import com.testtech.app.model.User;
import com.testtech.app.model.UserDao;

import java.util.Date;
import java.util.List;

public final class TestTechnology extends ActionBarActivity {

	private static TestTechnology ACTIVITY = null;

	private DaoMaster database;
	private Settings settings;
	private User session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// (Re)establish the activity singleton for global context access
		this.reloadSavedState(savedInstanceState);

		// Create the DAO context for this application
		final DaoMaster.DevOpenHelper dev_helper = new DaoMaster.DevOpenHelper(this, this.settings.getLocalDatabase().getDatabaseName(), null);
		final SQLiteDatabase db = dev_helper.getWritableDatabase();
		this.database = new DaoMaster(db);

		// Create base view (main + container).
		this.setContentView(R.layout.testtech_main);
		final FragmentManager manager = this.getSupportFragmentManager();
		manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

			@SuppressLint("NewApi")
			@Override
			public void onBackStackChanged() {

				Log.d("Test Technology", "Updating back stack...");
				final ActionBarActivity self = TestTechnology.this;
				self.getSupportActionBar().setDisplayHomeAsUpEnabled(manager.getBackStackEntryCount() > 0);

				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH)
					self.getSupportActionBar().setHomeButtonEnabled(manager.getBackStackEntryCount() > 0);
			}

		});

		if (savedInstanceState == null)
			this.endSession();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putSerializable(BundleKeyset.TT_ACTIVITY_SETTINGS, this.settings);

		if (this.session != null)
			outState.putSerializable(BundleKeyset.TT_ACTIVITY_SESSION, this.session);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home: {

				final FragmentManager manager = this.getSupportFragmentManager();
				if (manager.getBackStackEntryCount() > 0)
					manager.popBackStackImmediate();

				return true;
			}
			default: break;
		}

		return super.onOptionsItemSelected(item);
	}
	@Override
	protected void onResume() {

		super.onResume();

		// Check if user was logged in. If there is a user, load the session back directly.
		final SharedPreferences stored_session = this.getPreferences(Context.MODE_PRIVATE);
		if (stored_session != null) {

			final long id = stored_session.getLong(BundleKeyset.TT_ACTIVITY_STORED_SESSION, -1L);
			if (id != -1L) {

				final UserDao users = this.database.newSession().getUserDao();
				final List<User> session = users.queryBuilder()
												.where(UserDao.Properties.Id.eq(id))
												.limit(1)
												.list();

				if (session != null && !session.isEmpty()) {

					final User user = session.get(0);

					Log.i("Test Technology", "Restoring user session (id: " + user.getId() + ", username: " + user.getUsername() + ").");
					this.beginSession(user);
				}
				// TODO: Remove when no longer in test.
				else if (id == 0L) {

					Log.i("Test Technology", "Restoring user session (id: 0L, username: _root).");
					this.beginSession(new User(0L, "_root", "me@example.com", new Date().getTime(), new Date().getTime(), 0L));
				}
				// TODO: End remove
			}
		}
	}
	@Override
	protected void onPause() {

		super.onPause();

		final SharedPreferences stored_session = this.getPreferences(Context.MODE_PRIVATE);
		if (stored_session != null) {

			if (this.session != null) {

				Log.i("Test Technology", "Storing user session (id: " + this.session.getId() + ", username: " + this.session.getUsername() + ").");
				stored_session.edit().putLong(BundleKeyset.TT_ACTIVITY_STORED_SESSION, this.session.getId()).apply();
			}
			else {

				Log.i("Test Technology", "Unregistering any active session.");
				stored_session.edit().remove(BundleKeyset.TT_ACTIVITY_STORED_SESSION).apply();
			}
		}
	}

	public DaoMaster getDatabase() {
		return this.database;
	}
	public Settings getSettings() {
		return this.settings;
	}
	public User getSession() {
		return this.session;
	}

	public void beginSession(User session) {

		this.session = session;

		if (this.session != null) {

			final FragmentManager manager = this.getSupportFragmentManager();
			this.unlockDrawer();

			final Fragment dashboard = new DashboardFragment();
			manager.beginTransaction()
				   .replace(R.id.container, dashboard, "dashboard")
				   .commit();

			final Fragment drawer = new DashboardDrawerFragment();
			manager.beginTransaction()
				   .replace(R.id.drawer, drawer, "nav-drawer")
				   .commit();

		}
	}
	public void endSession() {

		if (this.session != null) {
			// TODO: Add a task to notify the server on the synchronization date.
		}

		this.session = null;
		this.lockDrawer();

		final FragmentManager manager = this.getSupportFragmentManager();
		final Fragment login = new LoginFragment();
		final Fragment drawer = manager.findFragmentByTag("nav-drawer");
		final FragmentTransaction t = manager.beginTransaction()
											 .replace(R.id.container, login, "login");

		if (drawer != null)
			t.remove(drawer);

		t.commit();
	}

	public void lockDrawer() {
		((DrawerLayout) (this.findViewById(R.id.main))).setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}
	public void unlockDrawer() {
		((DrawerLayout) (this.findViewById(R.id.main))).setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
	}
	private void reloadSavedState(Bundle inState) {

		if (inState != null) {

			this.settings = ((Settings) (inState.getSerializable(BundleKeyset.TT_ACTIVITY_SETTINGS)));
			this.session = ((User) (inState.getSerializable(BundleKeyset.TT_ACTIVITY_SESSION)));
		}
		else
			this.settings = Settings.createSettings(this);

		TestTechnology.ACTIVITY = this;
	}

	public static TestTechnology getActivity() {
		return TestTechnology.ACTIVITY;
	}

}
