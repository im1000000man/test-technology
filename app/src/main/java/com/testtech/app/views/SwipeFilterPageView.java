package com.testtech.app.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.testtech.app.BundleKeyset;
import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.fragments.FilterPageFragment;
import com.testtech.app.model.Filter;
import com.testtech.app.model.User;
import com.testtech.app.net.AAJAXTask;
import com.testtech.app.net.SurveyListSyncTask;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class SwipeFilterPageView extends SwipeRefreshLayout {

	private final static class FilterPageTransformer implements ViewPager.PageTransformer {

		@Override
		public void transformPage(View page, float position) {

			final float normalized_position = Math.abs(Math.abs(position) - 1);
			page.setScaleX((normalized_position / 2.0f) + 0.5f);
			page.setScaleY((normalized_position / 2.0f) + 0.5f);
		}

	}
	public final static class FilterPageAdapter extends FragmentStatePagerAdapter {

		private List<Filter> filters;
		private SwipeFilterPageView swipe_filter;

		public FilterPageAdapter(FragmentManager fm, SwipeFilterPageView swipe_filter, List<Filter> filters) {

			super(fm);
			this.swipe_filter = swipe_filter;
			this.filters = filters;

			if (this.filters == null)
				this.filters = new ArrayList<>(0);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return this.filters.get(position).getName().toUpperCase();
		}
		@Override
		public Fragment getItem(int position) {

			final Fragment f = new FilterPageFragment();

			final Bundle args = new Bundle();
			args.putSerializable(BundleKeyset.TT_FILTERED_LIST_FILTER, this.filters.get(position));
			args.putByte(BundleKeyset.TT_FILTERED_LIST_LAYOUT_MODE, this.swipe_filter.getPageLayoutMode());
			f.setArguments(args);

			return f;
		}
		@Override
		public int getItemPosition(Object object) {
			return FragmentStatePagerAdapter.POSITION_NONE;
		}
		@Override
		public int getCount() {
			return this.filters.size();
		}

	}
	private final static class SavedState extends BaseSavedState {

		public final static Creator<SavedState> CREATOR = new Creator<SavedState>() {

			@Override
			public SavedState createFromParcel(Parcel source) {
				return new SavedState(source);
			}
			@Override
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}

		};

		private int pager_position;
		private byte layout_mode;

		private SavedState(Parcel source) {

			super(source);
			this.pager_position = source.readInt();
			this.layout_mode = source.readByte();
		}
		public SavedState(Parcelable superState) {
			super(superState);
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {

			super.writeToParcel(dest, flags);
			dest.writeInt(this.pager_position);
			dest.writeByte(this.layout_mode);
		}

	}

	private static SwipeFilterPageView SWIPE = null;
	public final static byte
		LAYOUT_LIST = 0x01,
		LAYOUT_GRID = 0x02
	;

	private ViewPager pager;
	private byte layout_mode = SwipeFilterPageView.LAYOUT_LIST;

	public SwipeFilterPageView(Context context) {
		this(context, null);
	}
	public SwipeFilterPageView(final Context context, AttributeSet attrs) {

		super(context, attrs);
		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.swipe_filter_pager, this, true);

		SwipeFilterPageView.SWIPE = this;

		// Configure refreshing
		this.setColorScheme(R.color.refresh_1, R.color.refresh_2, R.color.refresh_3, R.color.refresh_4);
		this.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

			@Override
			public void onRefresh() {

				final SwipeFilterPageView swipe = SwipeFilterPageView.this;
				if (swipe.isRefreshing()) {

					final Settings settings = TestTechnology.getActivity().getSettings();
					final User session = TestTechnology.getActivity().getSession();

					final SurveyListSyncTask task = new SurveyListSyncTask(context, (settings.getServer().getHostURL() + "survey-list/5"), session);
					task.addResponseListener(new AAJAXTask.AAJAXResponseListener() {

						@Override
						public void onResponse(Context context, String response) {
							swipe.setRefreshing(false);
						}

					});
					task.execute(
						new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
						new BasicNameValuePair("server-key", settings.getServer().getDeviceServerKey()),
						new BasicNameValuePair("user-id", "" + session.getId()),
						new BasicNameValuePair("last-sync", "" + session.getLastSynchronizationDate())
					);
				}
			}

		});

		// Set view pager to use the filter list we just obtained.
		this.pager = ((ViewPager) (this.findViewById(R.id.pager)));
		this.pager.setPageTransformer(false, new FilterPageTransformer());
	}
	@Override
	protected Parcelable onSaveInstanceState() {

		Parcelable p = super.onSaveInstanceState();
		final SavedState outState = new SavedState(p);

		outState.layout_mode = this.layout_mode;
		outState.pager_position = this.pager.getCurrentItem();

		return outState;
	}
	@Override
	protected void onRestoreInstanceState(Parcelable state) {

		if (!(state instanceof SavedState)) {

			super.onRestoreInstanceState(state);
			return;
		}

		final SavedState inState = ((SavedState) (state));
		super.onRestoreInstanceState(inState.getSuperState());

		this.layout_mode = inState.layout_mode;
		this.pager.setCurrentItem(inState.pager_position);
	}

	public byte getPageLayoutMode() {
		return this.layout_mode;
	}
	public ViewPager getViewPager() {
		return this.pager;
	}

	public void setPageLayoutMode(byte layout_mode) {

		this.layout_mode = layout_mode;
		this.pager.getAdapter().notifyDataSetChanged();
	}

	@Override
	public void invalidate() {

		this.pager.getAdapter().notifyDataSetChanged();
		super.invalidate();
	}

	public static SwipeFilterPageView getInstance() {
		return SwipeFilterPageView.SWIPE;
	}

}
