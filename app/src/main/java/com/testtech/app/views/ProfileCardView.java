package com.testtech.app.views;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.testtech.app.R;
import com.testtech.app.fragments.GeneralProfileFragment;
import com.testtech.app.fragments.PersonalProfileFragment;

public class ProfileCardView extends RelativeLayout {

	public final static class ProfileHeaderView extends LinearLayout {

		public ProfileHeaderView(Context context) {
			this(context, null);
		}
		public ProfileHeaderView(Context context, AttributeSet attrs) {

			super(context, attrs);
		}

	}
	public final static class ProfileCardAdapter extends FragmentPagerAdapter {

		private final Fragment left, right;

		public ProfileCardAdapter(FragmentManager fm) {

			super(fm);
			this.left = new PersonalProfileFragment();
			this.right = new GeneralProfileFragment();
		}

		@Override
		public Fragment getItem(int position) {

			if (position == 0)
				return this.left;

			return this.right;
		}
		@Override
		public CharSequence getPageTitle(int position) {

			if (position == 0)
				return "ACCOUNT";

			return "PROFILE";
		}
		@Override
		public int getItemPosition(Object object) {
			return FragmentPagerAdapter.POSITION_NONE;
		}
		@Override
		public int getCount() {
			return 2;
		}

	}

	private ViewPager pager;

	public ProfileCardView(Context context) {
		this(context, null);
	}
	public ProfileCardView(Context context, AttributeSet attrs) {

		super(context, attrs);
		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.profile_page, this, true);

		this.pager = ((ViewPager) (this.findViewById(R.id.pager)));
	}

	public ViewPager getViewPager() {
		return this.pager;
	}

}
