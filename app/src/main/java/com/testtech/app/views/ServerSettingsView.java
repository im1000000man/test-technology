package com.testtech.app.views;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.Toast;

import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.net.AAJAXTask;
import com.testtech.app.net.ServerKeyTask;

import org.apache.http.message.BasicNameValuePair;

public class ServerSettingsView extends TableLayout {

	public ServerSettingsView(Context context) {
		this(context, null);
	}
	public ServerSettingsView(final Context context, AttributeSet attrs) {

		super(context, attrs);
		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.server_settings, this, true);

		// Setting the "hostname"/"port" fields
		final Settings settings = TestTechnology.getActivity().getSettings();
		final EditText hostname = ((EditText) (this.findViewById(R.id.hostname)));
		hostname.setText(settings.getServer().getServerHostname());
		final EditText port = ((EditText) (this.findViewById(R.id.port)));
		port.setText("" + settings.getServer().getServerPort());

		// Set the "Using HTTPS connection" checkbox
		final CheckBox use_https = ((CheckBox) (this.findViewById(R.id.use_https)));
		use_https.setSelected(settings.getServer().usingHttpsConnection());

		// Setting the "username"/"password" fields.
		final EditText username = ((EditText) (this.findViewById(R.id.username)));
		username.setText(settings.getServer().getServerUsername());
		final EditText passwd = ((EditText) (this.findViewById(R.id.passwd)));
		passwd.setTypeface(Typeface.DEFAULT);
		passwd.setTransformationMethod(new PasswordTransformationMethod());

		// Set the "Save settings" button action
		final Button save_settings = ((Button) (this.findViewById(R.id.save_settings)));
		save_settings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final Settings.ServerInformation server = settings.getServer();
				this.validateAndSaveConfiguration(server);



				final ServerKeyTask login = new ServerKeyTask(context, (server.getHostURL() + "server/auth"));
				login.addResponseListener(new AAJAXTask.AAJAXResponseListener() {

					@Override
					public void onResponse(Context context, String response) {

						try {

							((FragmentActivity) (context)).getSupportFragmentManager()
														  .popBackStackImmediate();
						}
						catch (ClassCastException e) { Log.e("Test Technology", "Cannot retrieve fragment manager from current context."); }
					}

				});
				login.execute(
					new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
					new BasicNameValuePair("build-no", "" + Build.VERSION.SDK_INT),
					new BasicNameValuePair("server-user", server.getServerUsername()),
					new BasicNameValuePair("server-passwd", login.encode(server.getServerPassword()))
				);
			}
			private void validateAndSaveConfiguration(Settings.ServerInformation server) {

				final String _hostname = hostname.getText().toString().trim();
				if (_hostname.isEmpty()) {

					Log.w("Test Technology", "Provided an invalid port number for connection.");
					Toast.makeText(context, R.string.error_no_host, Toast.LENGTH_SHORT).show();
					return;
				}

				int _port = 0;
				try { _port = Integer.parseInt(port.getText().toString().trim()); }
				catch (NumberFormatException e) {

					Log.w("Test Technology", "Provided an invalid port number for connection.");
					Toast.makeText(context, R.string.error_no_port, Toast.LENGTH_SHORT).show();
					return;
				}

				final boolean _use_https = use_https.isSelected();

				final String _username = username.getText().toString().trim();
				if (_username.isEmpty()) {

					Log.w("Test Technology", "Provided an empty username for authentication.");
					Toast.makeText(context, R.string.error_no_server_user, Toast.LENGTH_SHORT).show();
					return;
				}

				final String _passwd = passwd.getText().toString().trim();
				if (_passwd.isEmpty()) {

					Log.w("Test Technology", "Provided an empty password for authentication.");
					Toast.makeText(context, R.string.error_no_server_passwd, Toast.LENGTH_SHORT).show();
					return;
				}

				server.setServerHostname(_hostname);
				server.setServerPort(_port);
				server.setHttpsConnection(_use_https);
				server.setServerUsername(_username);
				server.setServerPassword(_passwd);
				settings.save(context);
			}

		});
	}

}
