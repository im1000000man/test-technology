package com.testtech.app.views;

import android.content.Context;
import android.graphics.Typeface;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.testtech.app.R;
import com.testtech.app.Settings;
import com.testtech.app.TestTechnology;
import com.testtech.app.model.User;
import com.testtech.app.net.LoginTask;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.message.BasicNameValuePair;

import java.util.Date;

public class AccountView extends LinearLayout {

	private final OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			final AccountView view = AccountView.this;
			final Context context = view.getContext();
			final Settings settings = TestTechnology.getActivity().getSettings();
			final Settings.ServerInformation server = settings.getServer();

			if (server != null) {

				if (server.getDeviceServerKey() != null) {

					String username = null;
					if (view.hasUserData())
						username = view.getUserData().getUsername();
					else {

						final EditText user = ((EditText) (view.findViewById(R.id.username)));
						username = user.getText().toString().trim();
					}


					final EditText pass = ((EditText) (view.findViewById(R.id.passwd)));
					final String passwd = new String(Hex.encodeHex(DigestUtils.sha1(pass.getText().toString().trim())));


					final LoginTask login = new LoginTask(context, (server.getHostURL() + "user/auth/" + username));
					login.execute(
						new BasicNameValuePair("server-key", login.encode(server.getDeviceServerKey())),
						new BasicNameValuePair("device-id", settings.getDeviceID().toString()),
						new BasicNameValuePair("passwd", login.encode(passwd))
					);
				}
				else {

					// TODO: Remove this when out of testing.
					String username = null;
					if (!view.hasUserData()) {

						final EditText user = ((EditText) (view.findViewById(R.id.username)));
						username = user.getText().toString().trim();

						if (username.equals("_root")) {

							final EditText pass = ((EditText) (view.findViewById(R.id.passwd)));
							final String passwd = new String(Hex.encodeHex(DigestUtils.sha1(pass.getText().toString().trim())));

							if (passwd.equals("da39a3ee5e6b4b0d3255bfef95601890afd80709")) {

								Log.i("Test Technology", "Starting session for user (id: 0, username: _root).");
								TestTechnology.getActivity().beginSession(new User(0L, "_root", "me@example.com", new Date().getTime(), new Date().getTime(), 0L));
								return;
							}
						}
					}
					// TODO: End remove

					Log.e("Test Technology", "Cannot login: no device-server key is registered in device.");
					Toast.makeText(context, R.string.error_no_key, Toast.LENGTH_SHORT).show();
				}
			}
		}

	};
	private User user_data;

	public AccountView(Context context) {
		this(context, null);
	}
	public AccountView(Context context, AttributeSet attrs) {

		super(context, attrs);

		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.login_default, this, true);

		final EditText passwd = ((EditText) (this.findViewById(R.id.passwd)));
		passwd.setTypeface(Typeface.DEFAULT);
		passwd.setTransformationMethod(new PasswordTransformationMethod());

		final Button login = ((Button) (this.findViewById(R.id.login)));
		login.setOnClickListener(this.listener);
	}

	public User getUserData() {
		return this.user_data;
	}
	public boolean hasUserData() {
		return (this.user_data != null);
	}

	public void setUserData(User user) {

		this.user_data = user;

		if (this.hasUserData()) {

			final LayoutInflater inflater = LayoutInflater.from(this.getContext());
			inflater.inflate(R.layout.login_user, this, true);

			final TextView username = ((TextView) (this.findViewById(R.id.username)));
			username.setText(this.user_data.getUsername());

			final Button login = ((Button) (this.findViewById(R.id.login)));
			login.setOnClickListener(this.listener);
		}
	}

}
