package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.QuestionType;
import com.google.gson.JsonObject;



import java.lang.reflect.Type;

public class QuestionTypeSchema implements JsonSchema<QuestionType> {

    @Override
    public QuestionType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        final JsonObject questionType = json.getAsJsonObject();
        if (questionType.get("id").getAsLong() < 1)
            throw new JsonParseException("Illegal questiontype definition encountered: ID cannot be less than 1.");
        if (questionType.get("description").isJsonNull() || questionType.get("description").getAsString().isEmpty())
            throw new JsonParseException("Illegal questiontype definition encountered: description cannot be empty.");
        if (questionType.get("type").isJsonNull() || questionType.get("type").getAsString().isEmpty())
            throw new JsonParseException("Illegal questiontype definition encountered: type cannot be empty.");

        final long id = questionType.get("id").getAsLong();
        final String description = questionType.get("description").getAsString();
        final String type = questionType.get("type").getAsString();

        return new QuestionType(id, description, type);
    }
    @Override
    public JsonElement serialize(QuestionType src, Type typeOfSrc, JsonSerializationContext context) {
        return null;
    }

}
