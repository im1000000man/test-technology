package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.Profile;
import java.lang.reflect.Type;
import java.util.Date;

public class ProfileSchema implements JsonSchema<Profile> {

	@Override
	public Profile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

		final JsonObject profile = json.getAsJsonObject();

		if (!profile.has("id") || profile.get("id").getAsLong() == 0L)
			throw new JsonParseException("Illegal profile_page definition encountered: ID cannot be null.");

		final long id = profile.get("id").getAsLong();
		final String first_name = (profile.has("first-name") ? profile.get("first-name").getAsString() : null);
		final String last_name = (profile.has("last-name") ? profile.get("last-name").getAsString() : null);
		final Date birth_date = (profile.has("birth-date") ? new Date(profile.get("birth-date").getAsLong()) : null);
		final byte gender = (profile.has("gender") ? profile.get("gender").getAsByte() : Profile.GENDER_MALE);

		return new Profile(id, first_name, last_name, birth_date, gender, 0L);
	}
	@Override
	public JsonElement serialize(Profile src, Type typeOfSrc, JsonSerializationContext context) {

		final JsonObject profile = new JsonObject();
		profile.add("id", new JsonPrimitive(src.getId()));

		if (src.getFirstName() != null)
			profile.add("first-name", new JsonPrimitive(src.getFirstName()));
		if (src.getLastName() != null)
			profile.add("last-name", new JsonPrimitive(src.getLastName()));
		if (src.getBirthDate() != null)
			profile.add("birth-date", new JsonPrimitive(src.getBirthDate().getTime()));

		profile.add("gender", new JsonPrimitive(src.getGender()));

		return profile;
	}

}
