package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.Question;

import java.lang.reflect.Type;

/**
 * Created by grivhan on 14/05/14.
 */
public class QuestionSchema implements JsonSchema<Question> {

    @Override
    public Question deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        final JsonObject question = json.getAsJsonObject();

        if (question.get("id").getAsLong() < 1)
            throw new JsonParseException("Illegal question definition encountered: ID cannot be less than 1.");
        if (question.get("survey_id").getAsLong() < 1)
            throw new JsonParseException("Illegal question definition encountered: survey_id cannot be less than 1.");
        if (question.get("type").getAsLong() < 1)
            throw new JsonParseException("Illegal question definition encountered: type_id cannot be less than 1.");
        if (question.get("flavor_text").isJsonNull() || question.get("flavor_text").getAsString().isEmpty())
            throw new JsonParseException("Illegal user definition encountered: flavor_text cannot be empty.");
        if (question.get("required").isJsonNull())
            throw new JsonParseException("Illegal user definition encountered: required cannot be empty.");

        final long id = question.get("id").getAsLong();
        final long survey_id = question.get("survey_id").getAsLong();
        final long type_id = question.get("type_id").getAsLong();
        final String flavor_text = question.get("flavor_text").getAsString();
        final Boolean required = question.get("required").getAsBoolean();

        return new Question(id, survey_id, type_id, flavor_text, required);
    }

    @Override
    public JsonElement serialize(Question src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject question = new JsonObject();
        question.add("id", new JsonPrimitive(src.getId()));
        question.add("survey_id", new JsonPrimitive(src.getSurveyId()));
        question.add("type", new JsonPrimitive(src.getTypeId()));
        question.add("flavor_text", new JsonPrimitive(src.getFlavorText()));
        question.add("required", new JsonPrimitive(src.getRequired()));

        return question;
    }
}
