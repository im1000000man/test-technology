package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.User;
import java.lang.reflect.Type;

public class UserSchema implements JsonSchema<User> {

	@Override
	public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

		final JsonObject user = json.getAsJsonObject();

		if (user.get("id").getAsLong() < 1)
			throw new JsonParseException("Illegal user definition encountered: ID cannot be less than 1.");
		if (user.get("username").isJsonNull() || user.get("username").getAsString().isEmpty())
			throw new JsonParseException("Illegal user definition encountered: username cannot be empty.");
		if (user.get("email").isJsonNull() || user.get("email").getAsString().isEmpty())
			throw new JsonParseException("Illegal user definition encountered: email cannot be empty.");
		if (user.get("created").getAsLong() == 0)
			throw new JsonParseException("Illegal user definition encountered: creation date must be set to a value.");

		final long id = user.get("id").getAsLong();
		final String username = user.get("username").getAsString();
		final String email = user.get("email").getAsString();
		final long created = user.get("created").getAsLong();
		long last_sync = (user.has("last-sync") ? user.get("last-sync").getAsLong() : 0L);

		return new User(id, username, email, created, last_sync, 0L);
	}
	@Override
	public JsonElement serialize(User src, Type typeOfSrc, JsonSerializationContext context) {

		final JsonObject user = new JsonObject();
		user.add("id", new JsonPrimitive(src.getId()));
		user.add("username", new JsonPrimitive(src.getUsername()));
		user.add("email", new JsonPrimitive(src.getEmail()));
		user.add("created", new JsonPrimitive(src.getCreationDate()));

		if (src.getLastSynchronizationDate() > 0)
			user.add("last-sync", new JsonPrimitive(src.getLastSynchronizationDate()));

		return user;
	}

}
