package com.testtech.app.model;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;

import com.testtech.app.model.Question;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table question.
*/
public class QuestionDao extends AbstractDao<Question, Long> {

    public static final String TABLENAME = "question";

    /**
     * Properties of entity Question.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, long.class, "id", true, "id");
        public final static Property SurveyId = new Property(1, long.class, "surveyId", false, "survey_id");
        public final static Property TypeId = new Property(2, long.class, "typeId", false, "type_id");
        public final static Property FlavorText = new Property(3, String.class, "flavorText", false, "flavor_text");
        public final static Property Required = new Property(4, boolean.class, "required", false, "required");
    };

    private DaoSession daoSession;


    public QuestionDao(DaoConfig config) {
        super(config);
    }
    
    public QuestionDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'question' (" + //
                "'id' INTEGER PRIMARY KEY NOT NULL ," + // 0: id
                "'survey_id' INTEGER NOT NULL ," + // 1: surveyId
                "'type_id' INTEGER NOT NULL ," + // 2: typeId
                "'flavor_text' TEXT NOT NULL ," + // 3: flavorText
                "'required' INTEGER NOT NULL );"); // 4: required
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'question'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Question entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
        stmt.bindLong(2, entity.getSurveyId());
        stmt.bindLong(3, entity.getTypeId());
        stmt.bindString(4, entity.getFlavorText());
        stmt.bindLong(5, entity.getRequired() ? 1l: 0l);
    }

    @Override
    protected void attachEntity(Question entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Question readEntity(Cursor cursor, int offset) {
        Question entity = new Question( //
            cursor.getLong(offset + 0), // id
            cursor.getLong(offset + 1), // surveyId
            cursor.getLong(offset + 2), // typeId
            cursor.getString(offset + 3), // flavorText
            cursor.getShort(offset + 4) != 0 // required
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Question entity, int offset) {
        entity.setId(cursor.getLong(offset + 0));
        entity.setSurveyId(cursor.getLong(offset + 1));
        entity.setTypeId(cursor.getLong(offset + 2));
        entity.setFlavorText(cursor.getString(offset + 3));
        entity.setRequired(cursor.getShort(offset + 4) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Question entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Question entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getSurveyDao().getAllColumns());
            builder.append(" FROM question T");
            builder.append(" LEFT JOIN survey T0 ON T.'survey_id'=T0.'id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Question loadCurrentDeep(Cursor cursor, boolean lock) {
        Question entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Survey survey = loadCurrentOther(daoSession.getSurveyDao(), cursor, offset);
         if(survey != null) {
            entity.setSurvey(survey);
        }

        return entity;    
    }

    public Question loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Question> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Question> list = new ArrayList<Question>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Question> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Question> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
