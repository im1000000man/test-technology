package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.Option;

import java.lang.reflect.Type;

/**
 * Created by grivhan on 14/05/14.
 */
public class OptionSchema implements JsonSchema <Option>{
    @Override
    public Option deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject option = json.getAsJsonObject();

        if (option.get("id").getAsLong() < 1)
            throw new JsonParseException("Illegal option definition encountered: ID cannot be less than 1.");
        if (option.get("question_id").getAsLong() < 1)
            throw new JsonParseException("Illegal option definition encountered: ID cannot be less than 1.");
        if (option.get("survey_id").getAsLong() < 1)
            throw new JsonParseException("Illegal option definition encountered: ID cannot be less than 1.");
        if (option.get("flavor_text").isJsonNull() || option.get("flavor_text").getAsString().isEmpty())
            throw new JsonParseException("Illegal user definition encountered: flavor_text cannot be empty.");
        //No value check as it can be null

        final long id = option.get("id").getAsLong();
        final long question_id = option.get("question_id").getAsLong();
        final long survey_id = option.get("survey_id").getAsLong();
        final String flavor_text = option.get("flavor_text").getAsString();
        final String value = option.get("value").getAsString();


        return new Option(id, survey_id, question_id, flavor_text, value);

    }
    @Override
    public JsonElement serialize(Option src, Type typeOfSrc, JsonSerializationContext context) {

        final JsonObject option = new JsonObject();

        option.add("id", new JsonPrimitive(src.getId()));
	    option.add("survey-id", new JsonPrimitive(src.getSurveyId()));
        option.add("question_id", new JsonPrimitive(src.getQuestionId()));
        option.add("flavor_text", new JsonPrimitive(src.getFlavorText()));
        option.add("value", new JsonPrimitive(src.getValue()));

        return option;
    }

}
