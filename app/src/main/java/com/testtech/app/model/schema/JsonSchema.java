package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;

public interface JsonSchema<T> extends JsonSerializer<T>, JsonDeserializer<T> { ; }
