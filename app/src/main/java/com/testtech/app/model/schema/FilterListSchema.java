package com.testtech.app.model.schema;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.Filter;
import java.lang.reflect.Type;

public class FilterListSchema implements JsonSchema<Filter[]> {

	@Override
	public Filter[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		return new Filter[0];
	}
	@Override
	public JsonElement serialize(Filter[] src, Type typeOfSrc, JsonSerializationContext context) {
		return null;
	}

}
