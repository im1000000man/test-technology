package com.testtech.app.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.testtech.app.model.Survey;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table survey.
*/
public class SurveyDao extends AbstractDao<Survey, Long> {

    public static final String TABLENAME = "survey";

    /**
     * Properties of entity Survey.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, long.class, "id", true, "id");
        public final static Property Title = new Property(1, String.class, "title", false, "title");
        public final static Property Description = new Property(2, String.class, "description", false, "description");
        public final static Property Author = new Property(3, String.class, "author", false, "author");
        public final static Property MultipleAttemptsPermitted = new Property(4, boolean.class, "multipleAttemptsPermitted", false, "multiple_attempts");
        public final static Property CreationDate = new Property(5, java.util.Date.class, "creationDate", false, "created");
        public final static Property Answered = new Property(6, boolean.class, "answered", false, "answered");
    };

    private DaoSession daoSession;


    public SurveyDao(DaoConfig config) {
        super(config);
    }
    
    public SurveyDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'survey' (" + //
                "'id' INTEGER PRIMARY KEY NOT NULL ," + // 0: id
                "'title' TEXT NOT NULL ," + // 1: title
                "'description' TEXT NOT NULL ," + // 2: description
                "'author' TEXT NOT NULL ," + // 3: author
                "'multiple_attempts' INTEGER NOT NULL ," + // 4: multipleAttemptsPermitted
                "'created' INTEGER NOT NULL ," + // 5: creationDate
                "'answered' INTEGER NOT NULL );"); // 6: answered
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'survey'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Survey entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
        stmt.bindString(2, entity.getTitle());
        stmt.bindString(3, entity.getDescription());
        stmt.bindString(4, entity.getAuthor());
        stmt.bindLong(5, entity.getMultipleAttemptsPermitted() ? 1l: 0l);
        stmt.bindLong(6, entity.getCreationDate().getTime());
        stmt.bindLong(7, entity.getAnswered() ? 1l: 0l);
    }

    @Override
    protected void attachEntity(Survey entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Survey readEntity(Cursor cursor, int offset) {
        Survey entity = new Survey( //
            cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // title
            cursor.getString(offset + 2), // description
            cursor.getString(offset + 3), // author
            cursor.getShort(offset + 4) != 0, // multipleAttemptsPermitted
            new java.util.Date(cursor.getLong(offset + 5)), // creationDate
            cursor.getShort(offset + 6) != 0 // answered
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Survey entity, int offset) {
        entity.setId(cursor.getLong(offset + 0));
        entity.setTitle(cursor.getString(offset + 1));
        entity.setDescription(cursor.getString(offset + 2));
        entity.setAuthor(cursor.getString(offset + 3));
        entity.setMultipleAttemptsPermitted(cursor.getShort(offset + 4) != 0);
        entity.setCreationDate(new java.util.Date(cursor.getLong(offset + 5)));
        entity.setAnswered(cursor.getShort(offset + 6) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Survey entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Survey entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
