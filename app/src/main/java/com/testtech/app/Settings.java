package com.testtech.app;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.testtech.app.model.schema.JsonSchema;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class Settings implements Serializable {

	public final static class ServerInformation implements Serializable {

		private static final long serialVersionUID = 834534948227189594L;
		
		private String device_server_key;
		private Date date_registered;
		private String server_host = "localhost";
		private int server_port = 80;
		private String server_user = "root";
		private String server_passwd = "";
		private boolean use_https = false;

		private ServerInformation() { ; }

		public String getDeviceServerKey() {
			return this.device_server_key;
		}
		public Date getDateRegistered() {
			return this.date_registered;
		}
		public String getServerHostname() {
			return this.server_host;
		}
		public int getServerPort() {
			return this.server_port;
		}
		public String getServerUsername() {
			return this.server_user;
		}
		public String getServerPassword() {
			return this.server_passwd;
		}
		public boolean usingHttpsConnection() {
			return this.use_https;
		}

		public void setDeviceServerKey(String device_server_key) {
			this.device_server_key = device_server_key;
		}
		public void setDateRegistered(Date date_registered) {
			this.date_registered = date_registered;
		}
		public void setServerHostname(String server_host) {
			this.server_host = server_host;
		}
		public void setServerPort(int server_port) {
			this.server_port = server_port;
		}
		public void setServerUsername(String server_user) {
			this.server_user = server_user;
		}
		public void setServerPassword(String server_passwd) {
			this.server_passwd = new String(Hex.encodeHex(DigestUtils.sha1(server_passwd)));
        }
		protected void setHashedServerPassword(String server_passwd) {
			this.server_passwd = server_passwd;
		}
		public void setHttpsConnection(boolean use_https) {
			this.use_https = use_https;
		}

		public String getHostURL() {

			final StringBuilder str = new StringBuilder();

			if (this.use_https)
				str.append("https://");
			else
				str.append("http://");

			str.append(this.server_host);
			if (this.server_port != 80)
				str.append(':').append(this.server_port);

			str.append('/');
			return str.toString();
		}

	}
	public final static class LocalDatabaseInformation implements Serializable {

		private static final long serialVersionUID = 8446567846819194347L;

		private String db_name = "testtech";
		private int db_version = 2;

		public int getDatabaseVersion() {
			return this.db_version;
		}
		public String getDatabaseName() {
			return this.db_name;
		}

		public void setDatabaseName(String db_name) {
			this.db_name = db_name;
		}
		public void setDatabaseVersion(int db_version) {
			this.db_version = db_version;
		}

	}
	private final static class SettingsSchema implements JsonSchema<Settings> {

		@Override
		public Settings deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

			final JsonObject settings = json.getAsJsonObject();

			// Deserialize device ID
			final UUID device_id = UUID.fromString(settings.get("device-id").getAsString());

			// Deserialize remote server settings
			final JsonObject server = settings.get("server").getAsJsonObject();
			final ServerInformation srv = new ServerInformation();

			srv.setDateRegistered(server.has("date-registered") ? new Date(server.get("date-registered").getAsLong()) : null);
			srv.setServerHostname(server.get("hostname").getAsString());
			srv.setServerPort(server.get("port").getAsInt());
			srv.setHttpsConnection(server.get("use-https").getAsBoolean());
			srv.setServerUsername(server.get("username").getAsString());
			srv.setHashedServerPassword(server.get("passwd").getAsString());

			// Deserialize local database settings
			final JsonObject local = settings.get("local").getAsJsonObject();
			final LocalDatabaseInformation lcl = new LocalDatabaseInformation();

			lcl.setDatabaseName(local.get("name").getAsString());
			lcl.setDatabaseVersion(local.get("version").getAsInt());

			return new Settings(device_id, srv, lcl);
		}
		@Override
		public JsonElement serialize(Settings src, Type typeOfSrc, JsonSerializationContext context) {

			final JsonObject settings = new JsonObject();
			// Serialize device ID
			settings.add("device-id", new JsonPrimitive(src.getDeviceID().toString()));

			// Serialize remote server settings
			final JsonObject server = new JsonObject();
			final ServerInformation srv = src.getServer();

			if (srv.getDateRegistered() != null)
				server.add("date-registered", new JsonPrimitive(srv.getDateRegistered().getTime()));

			server.add("hostname", new JsonPrimitive(srv.getServerHostname()));
			server.add("port", new JsonPrimitive(srv.getServerPort()));
			server.add("use-https", new JsonPrimitive(srv.usingHttpsConnection()));
			server.add("username", new JsonPrimitive(srv.getServerUsername()));
			server.add("passwd", new JsonPrimitive(srv.getServerPassword()));

			if (srv.getDeviceServerKey() != null)
				server.add("server-key", new JsonPrimitive(srv.getDeviceServerKey()));

			settings.add("server", server);

			// Serialize local database settings
			final JsonObject local = new JsonObject();
			final LocalDatabaseInformation lcl = src.getLocalDatabase();

			local.add("name", new JsonPrimitive(lcl.getDatabaseName()));
			local.add("version", new JsonPrimitive(lcl.getDatabaseVersion()));

			settings.add("local", local);

			return settings;
		}

	}

	private static final long serialVersionUID = -3701022399991851271L;
	private static final String SETTINGS_JSON_FILE = "settings.json";

	private UUID device;
	private ServerInformation server;
	private LocalDatabaseInformation local;

	private Settings(UUID device, ServerInformation server, LocalDatabaseInformation local) {

		this.device = device;
		this.server = server;
		this.local = local;
	}

	public UUID getDeviceID() {
		return this.device;
	}
	public ServerInformation getServer() {
		return this.server;
	}
	public LocalDatabaseInformation getLocalDatabase() {
		return this.local;
	}

	protected void setDeviceID(UUID device) {
		this.device = device;
	}
	protected void setLocalDatabase(LocalDatabaseInformation local) {
		this.local = local;
	}
	protected void setServer(ServerInformation server) {
		this.server = server;
	}

	public void save(Context context) {

		final Gson json = new GsonBuilder().registerTypeAdapter(Settings.class, new SettingsSchema())
										   .create();

		final String settings_json = json.toJson(this);

		try {

			OutputStream stream = context.openFileOutput(Settings.SETTINGS_JSON_FILE, Context.MODE_PRIVATE);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream));
			writer.write(settings_json);
			writer.close();
		}
		catch (IOException e) { Log.e("Test Technology", "Failed to write contents to \"" + new File(context.getFilesDir(), Settings.SETTINGS_JSON_FILE).getAbsolutePath() + "\" from application."); }
	}

	public static Settings createSettings(Context context) {

		final File settings_json = new File(context.getFilesDir(), Settings.SETTINGS_JSON_FILE);
		if (settings_json.exists()) {

			try {

				final List<String> file = Files.readLines(settings_json, Charset.defaultCharset());
				final StringBuilder str = new StringBuilder();
				for (String s : file)
					str.append(s).append('\n');

				final Gson json = new GsonBuilder().registerTypeAdapter(Settings.class, new SettingsSchema())
												   .create();

				return json.fromJson(str.toString(), Settings.class);
			}
			catch (IOException e) { Log.w("Test Technology", "Failed to read contents of \"" + settings_json.getAbsolutePath() + "\" into application."); }

			Log.i("Test Technology", "Creating temporary server settings information...");
			return new Settings(UUID.randomUUID(), new ServerInformation(), new LocalDatabaseInformation());
		}
		else
			return new Settings(UUID.randomUUID(), new ServerInformation(), new LocalDatabaseInformation());
	}

}
